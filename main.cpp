#include<iostream>
	using namespace std;
	class Sensor_Controller
	{
	  class state *current;
	  public:
	    Sensor_Controller();
	    void setCurrent(state *s)
	    {
	        current = s;
	    }


	    void sensing();
	    void processing();
	};


	class state
	{
	  public:
		virtual void idle(Sensor_Controller *sc)
	    {
	        cout << "  Idle\n";
	    }
		virtual void sensing(Sensor_Controller *sc)
	    {
	        cout << "   Sensing Sensor\n";
	    }
		virtual void processing(Sensor_Controller *sc)
	    {
	        cout << " Sensing sensor\n";
	    }
	};


	void Sensor_Controller::sensing()
	{
	  current->sensing(this);
	}


	void Sensor_Controller::processing()
	{
	  current->processing(this);
	}


	class Sense: public state
	{
	    int temp = 20, hum = 40;
	  public:
	    Sense()
	    {
	        cout << "   Getting Temperature Sensor ";
	        cout << "   Getting Humidity Sensor ";
	     int a;
	                  for(a=0;a<5;a++)
	                    {
	                        cout << "Temperature Sensor value: " <<temp<<endl;
	                    }
	     int b;
	                 for(b=0;b<5;b++)
	                    {
	                        cout << "Humidity Sensor value: " <<hum<<endl;
	                    }
	    };
	    ~Sense()
	    {
	        cout << "   Sensing data \n";
	    };

	};


	class Process: public state
	{
	  public:
	    Process()
	    {
	        cout << " process started ";
	    };
	    ~Process()
	    {
	        cout << "   Processing finished\n";
	    };
    void sensing(Sensor_Controller *sc)
	    {
	        cout << "   Sensing to Processing";
	        sc->setCurrent(new Sense());
	        delete this;
	    }
	};




	void Process::sensing(Sensor_Controller *sc)
	{
	  cout << "   Sensing to Processing";
	  sc->setCurrent(new Process());
	  delete this;
	}




	Sensor_Controller::Sensor_Controller()
	{
	  current = new Process();
	  cout << "Idle state" ;
	}


	int main()
	{
	  void(Sensor_Controller:: *ptrs[])() =
	  {
	    Sensor_Controller::processing, Sensor_Controller::sensing
	  };
	  Sensor_Controller fsm;
	  int num;
	  while (1)
	  {
	    cout << "Enter the state to be done:";
	    cin >> num;
	    (fsm.*ptrs[num])();
	  }
	}
